package com.snapp.task

import android.os.Bundle
import android.util.Log
import androidx.activity.viewModels
import androidx.appcompat.app.AppCompatActivity
import androidx.navigation.NavController
import androidx.navigation.NavGraph
import com.snapp.task.data.StateVehicle
import com.snapp.task.utils.findMyNavController
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class MainActivity : AppCompatActivity() {

    private lateinit var navGraph: NavGraph
    private lateinit var navController: NavController

    private val mainViewModel: MainViewModel by viewModels()


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.main_activity)
        Log.e("MainActivity", "onCreate")

        if (savedInstanceState == null)
            mainViewModel.start()


        mainViewModel.viewState.observe(this, {
            when (it) {
                StateVehicle.LIST ->
                    setStartDestination(R.id.VehiclesListFragment)
                StateVehicle.MAP ->
                    setStartDestination(R.id.VehiclesMapFragment)

            }
        })

    }

    private fun setStartDestination(idDestinationFragment: Int) {
        navController = supportFragmentManager.findMyNavController(R.id.nav_host_fragment)
        navGraph = navController.navInflater.inflate(R.navigation.nav_graph)
        navGraph.startDestination = idDestinationFragment
        navController.graph = navGraph
    }
}