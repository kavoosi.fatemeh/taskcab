package com.snapp.task.utils

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.snapp.task.data.Vehicle
import com.snapp.task.data.VehicleEntity

fun <T> MutableLiveData<T>.asLiveData() = this as LiveData<T>

fun List<Vehicle>.mapToVehicleEntity() =
        this.map {
            VehicleEntity(
                    0,
                    it.type,
                    it.lat,
                    it.lng,
                    it.imgUrl,
            )
        }


fun List<VehicleEntity>.mapVehicleEntityToVehicle() = map {
    Vehicle(
            it.type,
            it.lat,
            it.lng,
            it.imgUrl,
    )
}
