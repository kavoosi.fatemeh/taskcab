package com.snapp.task.utils

import android.content.Context
import android.graphics.BitmapFactory
import android.net.ConnectivityManager
import android.net.NetworkCapabilities
import android.os.Build
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import java.net.URL


fun isOnline(context: Context): Boolean {
    var isConnected = false
    return (context.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager).let {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            val networkCapabilities = it.activeNetwork ?: return false
            val activeNetwork =
                    it.getNetworkCapabilities(networkCapabilities) ?: return false
            isConnected = when {
                activeNetwork.hasTransport(NetworkCapabilities.TRANSPORT_WIFI) -> true
                activeNetwork.hasTransport(NetworkCapabilities.TRANSPORT_CELLULAR) -> true
                activeNetwork.hasTransport(NetworkCapabilities.TRANSPORT_ETHERNET) -> true
                else -> false
            }
        } else {
            run {
                it.activeNetworkInfo?.run {
                    isConnected = when (type) {
                        ConnectivityManager.TYPE_WIFI -> true
                        ConnectivityManager.TYPE_MOBILE -> true
                        ConnectivityManager.TYPE_ETHERNET -> true
                        else -> false
                    }
                }
            }
        }
        isConnected

    }
}

@Suppress("BlockingMethodInNonBlockingContext")
suspend fun convertUrlToBitmap(url: String) = withContext(Dispatchers.IO) {
    BitmapFactory.decodeStream(URL(url).openConnection().getInputStream())
}