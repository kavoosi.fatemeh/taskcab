package com.snapp.task.utils

import android.view.View
import android.widget.ImageView
import androidx.fragment.app.FragmentManager
import androidx.navigation.NavController
import androidx.navigation.fragment.NavHostFragment
import com.squareup.picasso.Picasso

val <T> T.exhaustive: T
    get() = this


fun ImageView.loadUrl(url: String, placeholder: Int) {
    Picasso.get()
            .load(url)
            .fit()
            .centerCrop()
            .placeholder(placeholder)
            .into(this)

}

fun View.visibility(isShow: Boolean) {
    if (isShow)
        this.visibility = View.VISIBLE
    else
        this.visibility = View.INVISIBLE

}

fun View.visible() {
    this.visibility = View.VISIBLE

}

fun View.inVisible() {
    this.visibility = View.INVISIBLE

}

fun FragmentManager.findMyNavController(navHostId: Int): NavController =
        (this.findFragmentById(navHostId) as NavHostFragment).navController
