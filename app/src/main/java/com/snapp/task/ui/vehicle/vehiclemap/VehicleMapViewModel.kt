package com.snapp.task.ui.vehicle.vehiclemap

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.snapp.task.data.ResultState
import com.snapp.task.data.Vehicle
import com.snapp.task.repository.Repository
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class VehicleMapViewModel @Inject constructor(private val repository: Repository) : ViewModel() {
    private var _vehicles = MutableLiveData<List<Vehicle>>()
    private val _resultState = MutableLiveData<ResultState<List<Vehicle>>>()

    val vehiclesLiveData: LiveData<List<Vehicle>> = _vehicles
    val resultStateLiveData: LiveData<ResultState<List<Vehicle>>> = _resultState

    fun loadVehicles() {
        _resultState.value = ResultState.Loading(true)
        viewModelScope.launch {
            repository.onGetVehicles().apply {
                if (this is ResultState.Success)
                    _vehicles.value = data!!
                _resultState.value = this

            }
        }

    }
}