package com.snapp.task.ui.vehicle.vehiclelist

import androidx.lifecycle.ViewModel
import com.snapp.task.repository.Repository
import dagger.hilt.android.lifecycle.HiltViewModel
import javax.inject.Inject

@HiltViewModel
class VehicleListViewModel @Inject constructor(private val repository: Repository) : ViewModel() {

    val vehiclesCacheLiveData = repository.onGetVehicleFromCache()

}