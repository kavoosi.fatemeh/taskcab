package com.snapp.task.ui.vehicle.vehiclelist

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.snapp.task.R
import com.snapp.task.data.Vehicle
import com.snapp.task.utils.loadUrl

class VehicleAdapter : RecyclerView.Adapter<VehicleAdapter.VehicleViewHolder>() {
    var list = listOf<Vehicle>()
        set(value) {
            field = value
            notifyDataSetChanged()
        }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): VehicleViewHolder {
        val view =
                LayoutInflater.from(parent.context).inflate(R.layout.row_vehicle, parent, false)
        return VehicleViewHolder(view)
    }

    override fun onBindViewHolder(holder: VehicleViewHolder, position: Int) {
        holder.bind(list[position])

    }

    //__viewwHolder _______________________________________________________________________________

    class VehicleViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        private val tvType: TextView = view.findViewById(R.id.tv_type)
        private val imgVehicle: ImageView = view.findViewById(R.id.img_vehicle)

        fun bind(item: Vehicle) {
            item.apply {
                tvType.text = type
                imgVehicle.loadUrl(imgUrl, R.drawable.placeholder_vehicle)
            }
        }


    }

    override fun getItemCount(): Int {
        return list.size
    }
}