package com.snapp.task.ui.vehicle.vehiclelist

import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.snapp.task.R
import com.snapp.task.data.Vehicle
import com.snapp.task.databinding.VehicleListFragmentBinding
import dagger.hilt.android.AndroidEntryPoint


@AndroidEntryPoint
class VehiclesListFragment : Fragment() {
    private var _binding: VehicleListFragmentBinding? = null

    // This property is only valid between onCreateView and  onDestroyView
    private val binding get() = _binding!!

    private val viewModel: VehicleListViewModel by viewModels()
    private var vehicleAdapter = VehicleAdapter()
    private lateinit var layoutManager: LinearLayoutManager


    companion object {
        const val KEY_STATE_SCROLL = "pos_scroll"
        private var posistion = 0
        const val TAG = "VehiclesListFragment"
    }

    //_______________________________________________________________________________________

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        _binding = VehicleListFragmentBinding.inflate(inflater, container, false)
        initRecycler()

        return binding.root
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        viewModel.vehiclesCacheLiveData.observe(viewLifecycleOwner, {
            if (it.isEmpty())
                showEmpty()
            else {
                showVehicles(it)
            }
        })


    }

    //called before onStart
    override fun onViewStateRestored(savedInstanceState: Bundle?) {
        savedInstanceState?.let {
            posistion = savedInstanceState.getInt(KEY_STATE_SCROLL)
        }
        super.onViewStateRestored(savedInstanceState)
    }


    //called before onStop or after onStop
    override fun onSaveInstanceState(savedInstanceState: Bundle) {
        savedInstanceState.putInt(
            KEY_STATE_SCROLL,
            layoutManager.findFirstCompletelyVisibleItemPosition()
        )
        super.onSaveInstanceState(savedInstanceState)
    }


    private fun showEmpty() {
        showToast(requireContext(), getString(R.string.emptySnapp))
    }

    private fun initRecycler() {
        val rv = binding.rvVehicles
        layoutManager = LinearLayoutManager(
            requireContext(), RecyclerView.VERTICAL,
            false
        )

        rv.layoutManager = layoutManager
        rv.adapter = vehicleAdapter
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }

    private fun showVehicles(it: List<Vehicle>?) {
        it?.let {
            vehicleAdapter.list = it
            binding.rvVehicles.post(Runnable {
                layoutManager.scrollToPositionWithOffset(posistion, 0)

            })
        }

    }
}

fun showToast(context: Context, message: String) {
    Toast.makeText(context, message, Toast.LENGTH_LONG).show()
}