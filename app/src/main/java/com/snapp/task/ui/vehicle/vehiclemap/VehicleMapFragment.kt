package com.snapp.task.ui.vehicle.vehiclemap

import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.SupportMapFragment
import com.google.android.gms.maps.model.BitmapDescriptorFactory
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.LatLngBounds
import com.google.android.gms.maps.model.MarkerOptions
import com.snapp.task.R
import com.snapp.task.data.ResultState
import com.snapp.task.data.Vehicle
import com.snapp.task.utils.convertUrlToBitmap
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext


@AndroidEntryPoint
class VehicleMapFragment : Fragment(), OnMapReadyCallback {

    companion object {
        private var hasRotation = false
        const val TAG = "VehiclesMapFragment"

        const val KEY_LAT = "latitude"
        const val KEY_LNG = "longitude"
        const val KEY_ZOOM = "zoom"
    }

    //used for configurationChange
    private var saveStateZoom: Float = 0.0f
    private lateinit var saveStateLatLng: java.util.ArrayList<LatLng>


    private lateinit var googleMap: GoogleMap
    private val viewModel: VehicleMapViewModel by viewModels()

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        hasRotation = savedInstanceState != null
        return inflater.inflate(R.layout.vehicle_map_fragment, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        val mapFragment = childFragmentManager.findFragmentById(R.id.map) as SupportMapFragment?
        mapFragment?.retainInstance=true
        mapFragment?.getMapAsync(this)
        super.onViewCreated(view, savedInstanceState)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        viewModel.resultStateLiveData.observe(viewLifecycleOwner, {
            when (it) {
                is ResultState.Loading -> {
                    Log.e(TAG, "${it.isShow}")
                }
                is ResultState.Success -> {
                    Log.e(TAG, "${it.data}")
                }
                is ResultState.Error ->
                    Log.e(TAG, it.e)

            }
        })

        viewModel.vehiclesLiveData.observe(viewLifecycleOwner, {
            if (this::googleMap.isInitialized) {
                zoomToCenterPoints(mapVehiclesToPoints(it))
                showVehiclesOnMap(it)
            }
        })


    }

    override fun onSaveInstanceState(savedInstanceState: Bundle) {
        googleMap.cameraPosition.apply {
            savedInstanceState.putDouble(KEY_LAT, target.latitude)
            savedInstanceState.putDouble(KEY_LNG, target.longitude)
            savedInstanceState.putFloat(KEY_ZOOM, zoom)
        }
        super.onSaveInstanceState(savedInstanceState)
    }

    override fun onViewStateRestored(savedInstanceState: Bundle?) {
        savedInstanceState?.let {
            saveStateZoom = savedInstanceState.getFloat(KEY_ZOOM)
            saveStateLatLng = arrayListOf(
                LatLng(
                    savedInstanceState.getDouble(KEY_LAT),
                    savedInstanceState.getDouble(KEY_LNG)
                )
            )
        }
        super.onViewStateRestored(savedInstanceState)
    }


    override fun onMapReady(googleMap: GoogleMap?) {
        googleMap?.let {
            googleMap.setMinZoomPreference(12f)

            this.googleMap = googleMap
            googleMap.mapType = GoogleMap.MAP_TYPE_NORMAL

            if (!hasRotation) {
                viewModel.loadVehicles()
            } else {
                //zoom to latest position
                zoomToCenterPoints(saveStateLatLng)
                // show latest vehicles on map
                viewModel.vehiclesLiveData.value?.let {
                    showVehiclesOnMap(it)
                }
            }
        }
    }

    private fun showVehiclesOnMap(vehicles: List<Vehicle>?) {
        vehicles?.let {
            for (element in vehicles) {
                addVehicleInfoToMarkerMap(element)
            }
        }
    }

    private fun addVehicleInfoToMarkerMap(vehicle: Vehicle) {
        CoroutineScope(Dispatchers.IO).launch {
            val bitmapIcon = convertUrlToBitmap(vehicle.imgUrl)
            withContext(Dispatchers.Main) {
                googleMap.addMarker(
                    MarkerOptions().position(LatLng(vehicle.lat, vehicle.lng))
                        .title(vehicle.type)
                        .icon(
                            BitmapDescriptorFactory.fromBitmap(
                                bitmapIcon
                            )

                        )
                )
            }
        }
    }

    private fun mapVehiclesToPoints(vehicles: List<Vehicle>): ArrayList<LatLng> {
        val points = ArrayList<LatLng>()
        vehicles.map {
            points.add(LatLng(it.lat, it.lng))
        }
        return points
    }

    private fun zoomToCenterPoints(latLngs: ArrayList<LatLng>) {
        val boundsBuilder = LatLngBounds.Builder()
        for (latLngItem in latLngs) {
            boundsBuilder.include(latLngItem)
        }
        googleMap.moveCamera(CameraUpdateFactory.newLatLngBounds(boundsBuilder.build(), 250))

        //zoom to latest state of position zoom
        if (saveStateZoom > 0.0)
            googleMap.moveCamera(CameraUpdateFactory.zoomTo(saveStateZoom))

    }


}


