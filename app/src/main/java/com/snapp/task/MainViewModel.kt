package com.snapp.task

import android.content.Context
import android.util.Log
import androidx.lifecycle.ViewModel
import com.snapp.task.data.StateVehicle
import com.snapp.task.utils.isOnline
import com.taskmahsan.app.utility.SingleLiveData
import dagger.hilt.android.lifecycle.HiltViewModel
import dagger.hilt.android.qualifiers.ApplicationContext
import javax.inject.Inject

@HiltViewModel
class MainViewModel @Inject constructor(@ApplicationContext val context: Context) : ViewModel() {

    private val _stateViewVehicles = SingleLiveData<StateVehicle>()
    val viewState: SingleLiveData<StateVehicle> = _stateViewVehicles

    fun start() {
        Log.e("MainViewModel", "onCreate")

        if (isOnline(context))
            _stateViewVehicles.value = StateVehicle.MAP
        else
            _stateViewVehicles.value = StateVehicle.LIST
    }
}