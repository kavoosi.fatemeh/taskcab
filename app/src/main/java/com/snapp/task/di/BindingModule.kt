package com.snapp.task.di

import com.snapp.task.repository.Repository
import com.snapp.task.repository.RepositoryImpl
import dagger.Binds
import dagger.Module
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import javax.inject.Singleton

@Module
@InstallIn(SingletonComponent::class)
abstract class BindingModule {

    @Singleton
    @Binds
    abstract fun bindingNewsRepository(repositoryImpl: RepositoryImpl): Repository
}