package com.snapp.task.data

data class VehicleResponse(
    val vehicles: List<Vehicle>
)