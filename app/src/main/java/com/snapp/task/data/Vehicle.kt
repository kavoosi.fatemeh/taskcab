package com.snapp.task.data

import com.google.gson.annotations.SerializedName

data class Vehicle(
    val type: String,
    val lat: Double,
    val lng: Double,
    @SerializedName("image_url") val imgUrl: String,
)