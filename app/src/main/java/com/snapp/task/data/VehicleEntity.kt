package com.snapp.task.data

import androidx.room.Entity
import androidx.room.PrimaryKey
import com.google.gson.annotations.SerializedName

@Entity
data class VehicleEntity(
    @PrimaryKey(autoGenerate = true)
    val id: Int,
    val type: String,
    val lat: Double,
    val lng: Double,
    val imgUrl: String,
)