package com.snapp.task.repository.remote

import com.snapp.task.data.VehicleResponse
import retrofit2.Response
import retrofit2.http.GET

interface VehicleApi {
    @GET("/assets/test/document.json")
    suspend fun onGetVehicles(): Response<VehicleResponse>
}