package com.snapp.task.repository.local

import androidx.lifecycle.LiveData
import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.snapp.task.data.VehicleEntity

@Dao
interface VehicleDao {

    @Query("SELECT * FROM VehicleEntity ")
    fun getItems(): LiveData<List<VehicleEntity>>

    @Insert(onConflict = OnConflictStrategy.IGNORE)
    fun insertItems(items: List<VehicleEntity>)

    @Query("DELETE FROM VehicleEntity")
    suspend fun clearCache()
}