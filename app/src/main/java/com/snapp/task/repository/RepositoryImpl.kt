package com.snapp.task.repository

import androidx.lifecycle.Transformations
import com.snapp.task.data.ResultState
import com.snapp.task.data.Vehicle
import com.snapp.task.repository.local.VehicleDao
import com.snapp.task.repository.remote.VehicleApi
import com.snapp.task.utils.mapToVehicleEntity
import com.snapp.task.utils.mapVehicleEntityToVehicle
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import javax.inject.Inject

class RepositoryImpl @Inject constructor(
        private val vehicleApi: VehicleApi,
        private val vehicleDao: VehicleDao
) : Repository {
    override suspend fun onGetVehicles(): ResultState<List<Vehicle>> = withContext(Dispatchers.IO) {
        try {
            vehicleApi.onGetVehicles().let {
                when {
                    it.isSuccessful -> {
                        it.body()?.vehicles?.let { vehicles ->
                            deleteVehicles()
                            saveVehicles(vehicles)
                        }
                        ResultState.Success(it.body()?.vehicles)
                    }
                    else -> ResultState.Error(it.errorBody().toString())

                }


            }
        } catch (e: Exception) {
            ResultState.Error(e.message.toString())
        }


    }

    override fun onGetVehicleFromCache() =
            Transformations.map(vehicleDao.getItems()) {
                it.mapVehicleEntityToVehicle()
            }

    private suspend fun saveVehicles(vehicles: List<Vehicle>) =
            withContext(Dispatchers.IO) {
                vehicleDao.insertItems(vehicles.mapToVehicleEntity())

            }


    private suspend fun deleteVehicles() {
        withContext(Dispatchers.IO) {
            vehicleDao.clearCache()
        }
    }


}
