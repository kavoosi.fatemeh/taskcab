package com.snapp.task.repository.local

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import com.snapp.task.data.VehicleEntity

@Database(entities = [VehicleEntity::class], version = 1)
abstract class AppDatabase : RoomDatabase() {
    abstract fun vehicleDao(): VehicleDao

    companion object {

        fun getInstance(context: Context) = Room.databaseBuilder(
            context,
            AppDatabase::class.java, "AppDatabase"
        ).build()

    }
}