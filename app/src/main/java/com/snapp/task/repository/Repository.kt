package com.snapp.task.repository

import androidx.lifecycle.LiveData
import com.snapp.task.data.ResultState
import com.snapp.task.data.Vehicle

interface Repository {
    suspend fun onGetVehicles(): ResultState<List<Vehicle>>
    fun onGetVehicleFromCache(): LiveData<List<Vehicle>>
}